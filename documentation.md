### WebAppService Documentation  
### Documentation on how to use the service  
### Version: 1.03

![](images/image1.png)

**Content**

1. Overview  
  1.1 About this document  
  1.2 What is the WebAppService and what does it do?  
2. WebAppService Usage  
  2.1 Running the WebAppService  
  2.2 Connecting to the WebAppService  
  2.3 Communicating with the WebAppService  
3. Command list  
  3.1 HMI commands  
  3.2 Scanner Commands  
  3.3 M2Longrange  
  3.4 Hasci Commands  
  3.5 M2UHF Commands   
4. License Manager  
5. Support

## Overview

### About this document

This document is intended for developers programming a locally running web application that can communicate with ACD Hardware using the WebAppService. It explains how the WebAppService works, how the WebAppService can be used and contains a list of all the commands that can be received by the WebAppService.

### What is the WebAppService and what does it do?

The WebAppService is an Android package designed for ACD devices running Android 9 or newer.

The package contains a service and an application. The service is acting as the interface between the web application and the hardware component. The application is used to manage this service.

It needs to be installed and running on a device of choice to provide web applications the possibility to interact with supported hardware components.

The service hosts a web socket server to which a web application can be connected to. Once connected, the web application will be able to send commands to the WebAppService. The WebAppService then will
interpret those commands and will then communicate accordingly with the hardware component. After it is done executing, it will report back to the connected web application and will provide optional data from the hardware component.

![](images/Diagram.png)

*Figure 1: communication diagram*

Another thing that might be important to know is that the WebAppService does support multiple web applications to be connected to it at once. As soon as a client starts using a hardware component, it is
reserved by that client and can only be used by that client. The client will stay the reserved until the unbind command of that hardware component is called or the client disconnects from the WebAppService.

## WebAppService Usage

### Running the WebAppService

To be able to connect to the WebAppService, you first need to start the WebAppService on your Android device. To do that, you need to install the WebAppService and start the application of the WebAppService. There you can start and stop the WebAppService service using the corresponding buttons.

![](images/UI.png)

*Figure 2: WebAppService application*

It is recommended to activate the "Start service on device startup" toggle switch when using the WebAppService on a reular basis, because then it will start automatically on the devices startup and will be running all the time until it is stopped manually.

To see if the WebAppService Service is running, you can check if a notification with the title "WebAppService" is displayed. This notification also provides information on how many clients are connected to the service as soon as at least one is connected.

![](images/notification.png)

*Figure 3: information notification*

## Connecting to the WebAppService

The WebAppService is running a web socket server clients can connect to. This web socket server is locally accessible over the port 56665.

Here is an example on how connecting to the WebAppService can look like within JavaScript:

```js
var wsUri = "ws://127.0.0.1:56665/"; // IP of local host

var websocket = new WebSocket(wsUri);

websocket.onopen = function(evt) {

  // when the connection is established

};

websocket.onclose = function(evt) {

  // when the connection is lost

};

websocket.onmessage = function(evt) {

  // when receiving a message

};

websocket.onerror = function(evt) {

  // when an error occurs

};
```

## Communicating with the WebAppService

The communication with the WebAppService is achieved by sending string messages to the web socket. Those messages contain the hardware component they are targeting, a command that tells the hardware component what to do and optionally some parameters for the command. When the WebAppService receives the command it will execute it on the hardware component. Once it is done executing, the WebAppService will respond with a message also containing the hardware component, a command and optionally some additional data sent by the hardware component. Some commands also might respond multiple times.

All commands need to be formatted like this:

`[hardware component].[command].[data/parameter JSON]`

All data and parameters need to be formatted as JSON objects. The content of those JSON objects depend on the command. Please refer to chapter "3 Command list" to see what the command you want to use requires as parameters and what it will return after executing.

Here is an example of what a command without parameters can look like:

`HMI.bindHMI`

And here is an example of what a command with parameters can look like:

`HMI.changePassword.{"oldPass": "1234", "newPass": "4321"}`

To interpret a message received from the WebAppService, the hardware component, the command and the data need to be separated. To do that, the message needs to be split at the first and the second '.' character in the message string. It is important to know that the '.' character can appear within the data string. This means that you have to make shure to only split the message at the first two '.' characters, otherwise the data string might be cut off.

An example on how to get the correct data off a message can look like this in JavaScript:

```js
websocket.onmessage = function(evt) {

   var message = evt.data;

   var hardwareComponent = message.split('.', 2)[0];

   var command = message.split('.', 2)[1];

   var data = '';

   // separate the data using the substring function

   var dataStartPoint = hardwareComponent.length + command.length + 2;

   if (dataStartPoint < message.length) {

   data = message.substring(dataStartPoint, message.length)

   }

// your code

};
```

## Command list

This chapter contains all the commands the WebAppService can receive or will send. The commands all need to be according to the protocol described in "2.3 Communicating with the WebAppService". 

Every command can return an error string as data if there is an internal problem. If this is the case, all the other data usually returned isn't available.

### HMI commands

The HMI commands are directly redirected to the HMI service. Please refer to the documentation of the HMI service for more detailed information on individual commands and parameters.

| **Hardware component name: HMI** |  |  |
| --- | --- | --- |
| **Command** | **Sent by Client** | **Sent by WebAppService** |
||||
| **bindHMI**          |                      | **Data:** <br>- bound: boolean    |
| **unbindHMI**        |                      | **Data:** <br>- bound: boolean    |
| **subscribeToButtonStatus** | **Parameters:** <br>- arg1: int |                 |
| **subscribeToMessages** |                      |                      |
| **sendNewSafetyParameters** | **Parameters:** <br>- name: string <br>- ERL: boolean <br>- offset: int <br>- warning_zone: int <br>- timeout: int <br>- pass: string | |
| **appGuardAdd**      | **Parameters:**<br>- ADD: string      |                      |
| **appGuardRemove**   | **Parameters:**<br>- REMOVE: string      |                      |
| **appGuardEnable**   | **Parameters:**<br>- arg1: int      |                      |
| **connectionSignal** |                      |                      |
| **requestRSSI**      |                      |                      |
| **appGuardList**     |                      |                      |
| **changePassword**          | **Parameters:**<br>- oldPass: string<br>- newPass: string    |                           |
| **disconnect**              |                    |                           |
| **confirmSafetyParameters** | **Parameters:**<br>- arg1: int    |                           |
| **requestRevisionData**     |                    |                           |
| **requestConnectionStatus** |                    |                           |
| **requestParams**           |                    |                           |
| **parameterSet**            |          N/A          | **Data:**<br>- arg1: int<br>- name: string<br>- intensity: int<br>- timeout: int<br>- reset: bool <br>- rangeOn: bool          |
| **warning/error**           |          N/A          | **Data:**<br>- arg1: int<br>- arg2: int              |
| **connectionStatus**        |          N/A          | **Data:**<br>- arg1: int<br>- arg2: int<br>- rangeOn: bool          |
| **revisionData**            |          N/A          | **Data:**<br>- SWVerModCh1: string<br>- SWVerModCh2: string<br>- SWVerGateCh1: string<br>- SWVerGateCh2: string<br>- HWVerMod: string<br>- HWVerGate: string<br>- RevisionMod: string<br>- RevisionGate: string<br>- SNoMod: string<br>- MConVer: string<br>- ServiceVersion: string<br>- AmspVersion: string    |
| **buttonStatus**            |          N/A          | **Data:**<br>- In-ES: bool<br>- In-EB: bool<br>- GW-ES-ch1: bool<br>- GW-ES-ch2: bool<br>- GW-EB-ch1: bool<br>- GW-EB-ch2: bool<br>- intensity: int         |
|  **readBackSafetyParameters**      | N/A  |      |
|  **parameterReleaseAck**           | N/A  |      |
|  **parameterReleaseError**         | N/A  |      |
|  **disconnecting**                 | N/A  |      |
|  **cancelDisconnection**           | N/A  |      |
|  **passwordChanged**               | N/A  |      |
|  **responseRSSI**                  | N/A  |      |

### Scanner Commands

The scanner commands are directly redirected to the ACD ScannerService. Please refer to the ACD Android Programming Manual for more detailed information on individual commands and parameters.

| **Hardware component name: Scanner**      |                      |                      |
|---|---|---|
| **Command**          | **Sent by Client**   | **Sent by WebAppService**       |
||||
| **bindScanner**      |                      |                      |
| **unbindScanner**    |                      |                      |
| **triggerScan**      | **Parameters:**<br>- arg1: int<br>(sending parameters here will still result in triggering a scan)   |                      |
| **setTarget**        | **Parameters:**<br>- arg1: int         |                      |
| **keyEnable**        | **Parameters:**<br>- arg1: int         |                      |
| **aim**              | **Parameters:**<br>- arg1: int         |                      |
| **ok**               |         N/A             | **Data:**<br>- arg1: int<br>- arg2: int         |
| **hello**            |         N/A             |                      |
| **barcode**          |         N/A             | **Data:**<br>- data: bundle      |

### M2Longrange

The M2Longrange commands are directly redirected to the ACD ScannerServiceLR. Please refer to the ACD Android programming Manual for more detailed information on individual commands and parameters.

| **Hardware component name: M2Longrange**  |                      |                      |
|---|---|---|
| **Command**          | **Sent by Client**   | **Sent by WebAppService**       |
||||
| **bindLR**           |                      |                      |
| **unbindLR**         |                      |                      |
| **triggerScan**      | **Parameters:**<br>- arg1: int<br>(sending no parameters here will still result in triggering a scan)   |                      |
| **setTarget**        | **Parameters:**<br>- arg1: int         |                      |
| **keyEnable**        | **Parameters:**<br>- arg1: int         |                      |
| **Aim**              | **Parameters:**<br>- arg1: int         |                      |
| **ok**               |        N/A              | **Data:**<br>- arg1: int<br>- arg2: int         |
| **hello**            |        N/A              |                      |
| **barcode**          |        N/A              | **Data:**<br>- data: bundle      |

### Hasci Commands

The Hasci commands are directly redirected to the ACD HasciDataService. Please refer to the ACD HasciDataService documentation for more detailed information on individual commands and parameters.

| **Hardware componentname: Hasci**        |                      |                      |
|---|---|---|
| **Command**          | **Sent by Client**   | **Sent by WebAppService**       |
| **bindDataService**  |                      |                      |
| **unbindDataService** |                      |                      |
| **connectHasci**     |                      |                      |
| **config**           | **Parameters:**<br>- configCommand:    |                      |
|                      | string<br><br>If configCommand is SYMENABLE:<br>- permanent: boolean<br>- enable: boolean <br>- barcodeType: string<br><br>If configCommand is SYMSTATUS:<br>- barcodeType: string<br><br>If configCommand is AUTOTRIGGER:<br>- permanent: boolean<br>- enable: boolean<br><br>If configCommand is SCANDELAY:<br>- permanent: boolean<br>- duration: int<br><br>If configCommand is VIBRATE:<br>- duration: int<br><br>If configCommand is BEEP:<br>- frequency: int<br>- duration: int     |                      |
| **attributes**       | **Parameters:**<br>- command: string   |                      |
| **update**           | **Parameters:**<br>- command: string<br>- filename: string  |                      |
| **ok**               |      N/A                |                      |
| **error**            |      N/A                |                      |
| **hello**            |      N/A                |                      |
| **scanData**         |      N/A                |                      |
| **updateResponse**   |      N/A                |                      |

### M2UHF Commands

The M2UHF commands are directly redirected to the ACD M2UHF-RFID API. Please refer to the ACD M2UHF-RFID API documentation for more detailed information on individual commands and parameters.

| **Hardware component name: M2UHF**         |                    |                       |
|---|---|---|
| **Command**           | **Sent by Client** | **Sent by WebAppService**        |
| **bindModule**        |                    |                       |
| **unbindModule**      |                    | **Data:**<br>- commandSent: boolean               |
| **modulePowerOn**     |                    | **Data:**<br>- commandSent: boolean               |
| **modulePowerOff**    |                    | **Data:**<br>- commandSent: boolean               |
| **moduleCheckPowerStatus**                |                    | **Data:**<br>- commandSent: boolean               |
| **moduleIsPlugged**   |                    | **Data:**<br>- plugged: boolean   |
| **moduleIsPowered**   |                    | **Data:**<br>- powered: boolean   |
| **moduleGetStatus**   |                    | **Data:**<br>- status: Ipj_Error  |
| **moduleGetMconVersion**                   |                    | **Data:**<br>- mconVersion: string                |
| **readerInit**        |                    | **Data:**<br>- initialized: Ipj_Error             |
| **readerDeInit**      |                    | **Data:**<br>- deinitialized: Ipj_Error             |
| **readerStart**       |                    | **Data:**<br>- start: Ipj_Error   |
| **readerStop**        |                    | **Data:**<br>- stop: Ipj_Error    |
| **readerWriteTag**   | **Paremeters:**<br>- tagToSelect: int[]<br>- selectedTagIsTID: boolean<br>- dataToWrite: int[]<br>- memBankToWrite: MemBank<br>- index: int<br>- password: long  | **Data:**<br>- writeTag: Ipj_Error        |
| **readerReadTag**    | **Paremeters:**<br>- tagToSelect: int[]<br>- selectedTagIsTID: boolean<br>- memBankToWrite: MemBank<br>- index: int<br>- count: int<br>- password: long    | **Data:**<br>- readTag: Ipj_Error  |
| **readerSetLockOnTag**                   | **Paremeters:**<br>- tagToSelect: int[]<br>- selectedTagIsTID: boolean<br>- lockMode: LockMode<br>- password: long    | **Data:**<br>- setLockOnTag: Ipj_Error |
| **readerSetLockOnMemBank** | **Paremeters:**<br>- tagToSelect: int[]<br>- selectedTagIsTID: boolean<br>- memBankToLock: MemBank<br>- lockMode: LockMode<br>- password: long    | **Data:**<br>- setLockOnMemBank: Ipj_Error |
| **readerKillTag**    | **Parameters:**<br>- tagToSelect: int[]<br>- selectedTagIsTID: boolean<br>- password: long    | **Data:**<br>- killTag: Ipj_Error  |
| **readerEnableCallback** |                      | **Data:**<br>- callbackEnabled: boolean              |
| **readerEnableAdvancedCallback**|   | **Data:**<br>-advancedCallbackEnabled: boolean              |
| **readerIsInitialized** |  | **Data:**<br>- initialized: boolean              |
| **getDeviceType**    |  | **Data:**<br>- type: DeviceType  |
| **getDeviceCert**    |  | **Data:**<br>- certification: DeviceCertification  |
| **getVersion**       |  | **Data:**<br>- version: string   |
| **getTxPower**       |  | **Data:**<br>- txPower: int      |
| **getRFMode**        |  | **Data:**<br>- rfMode: int       |
| **getSession**       |  | **Data:**<br>- session: int      |
| **getRegion**        |  | **Data:**<br>- region: Regulatory_Region    |
| **getTagPopulation** |  | **Data:**<br>- population: int   |
| **getSearchMode**    |  | **Data:**<br>- searchMode: int   |
| **getKeyInfo**       | **Parameters:**<br>- keyToGet: Ipj_Key      | **Data:**<br>- keyInfo: KeyInfo  |
| **getKeyValue**      | **Parameters:**<br>- keyToGet: Ipj_Key | **Data:**<br>- keyValue: int     |
| **getKey**           | **Parameters:**<br>- keyToGet: Ipj_Key<br>- bankIndex: int<br>- valueIndex: int   | **Data:**<br>- key: int |
| **setTxPower**       | **Parameters:**<br>- power: int | **Data:**<br>- setTxPower: Ipj_Error            |
| **setRFMode**        | **Parameters:**<br>- mode: int | **Data:**<br>- setRFMode: Ipj_Error            |
| **setSession**       | **Parameters:**<br>- session: int | **Data:**<br>- setSession: Ipj_Error            |
| **setTagPopulation** | **Parameters:**<br>- rate: int | **Data:**<br>- setTagPopulation: Ipj_Error            |
| **setSearchMode**    | **Parameters:**<br>- mode: int | **Data:**<br>- setSearchMode: Ipj_Error            |
| **setEtsiRegion**    | **Parameters:**<br>- etsiRegion: Regulatory_Region | **Data:**<br>- setEtsiRegion: Ipj_Error            |
| **setKeyValue**      | **Parameters:**<br>- keyToSet: Ipj_Key<br>- value: int        | **Data:**<br>- setKeyValue: Ipj_Error |
| **setKey**           | **Parameters:**<br>- keyToSet: Ipj_Key<br>- bankIndex: int      | **Data:**<br>- setKey: Ipj_Error |
| **moduleStatusReporter** | N/A  | **Data:**<br>- onModulePlugged: boolean<br>- onModulePowered: boolean<br>- onModuleError: Ipj_Error                    |
| **jniReporter**          |N/A   | **Data:**<br>- onStatusMessage: Ipj_Error<br>- onTagUpdate: boolean<br>- EPC: string<br>- TID: string<br>- RSSI: int<br>- phase: int<br>- pc: int<br>- onTagStatus: int                            |
| **advancedJniReporter**  | N/A  | **Data:**<br>- onTagOperationReport: ipjTagOperationReport |

## License Manager

A valid license is required to operate this application. When you start the app for the first time, a demo license is automatically activated, which is valid for a period of two months. Once the demo license has expired, the license manager opens automatically when you start the app. Here you can now enter a new, valid license. To activate a new license, enter the license key in the “Enter license key” field. Then press the “Activate license” button to activate the license.

<img src="images/license1.png" alt="License Manager" width="720"/>

*Figure 4: License Manager*

If there is no valid license, the ACD app can no longer be used. In this case, please contact ACD Support (see Chapter **5 Support**) or your sales contact.
The license manager can also be accessed manually via the ACD SystemApp. To do this, navigate to the “Licenses” menu item in the ACD SystemApp and select the desired app. Please note that the corresponding ACD app must be installed on the device to manage its license.

<img src="images/license2.png" alt="License Manager" width="720"/>

*Figure 5: System App*

## Support
If you need support, please contact our support hotline.  

**/// ACD Elektronik GmbH**  
Engelberg 2  
88480 Achstetten, Germany  
Tel.: +49 7392 708-488  
E-mail: support.technik@acd-elektronik.de  
Web: https://www.acd-gruppe.de/en/  

The support hotline is available for you Monday to Thursday from 8:00 am to 5:00 pm and
Friday from 8:00 am to 3:00 pm.